import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ILoadedEventArgs, ChartTheme } from '@syncfusion/ej2-angular-charts';
import {
  ApexNonAxisChartSeries,
  ApexPlotOptions,
  ApexChart,
  ApexFill,
  ChartComponent,
  ApexStroke,
  ApexAxisChartSeries,
  ApexXAxis,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexGrid
} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  labels: string[];
  plotOptions: ApexPlotOptions;
  fill: ApexFill;
  stroke: ApexStroke;
};
export type ChartOptions2 = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  dataLabels: ApexDataLabels;
  grid: ApexGrid;
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
};

@Component({
  selector: 'app-bi',
  templateUrl: './bi.component.html',
  styleUrls: ['./bi.component.scss']
})
export class BiComponent implements OnInit {
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + window.localStorage.getItem('ESP_access_token')
    })
  }
  @ViewChild("chart") chart: ChartComponent;
  @ViewChild("charttable") charttable: ChartComponent;
  @ViewChild("charttabla") charttabla: ChartComponent;

  // public chartOptions: Partial<ChartOptions>;

  constructor(private http: HttpClient) { 
  }

  ngOnInit() {
    this.getStatsGlobal()
    this.getObjectifsRange()
    this.getObjectifs(this.defaultData())
    
  }

  public chartOptions: any = {
    series: [0],
    chart: {
      height: 150,
      width:150,
      type: "radialBar",
      offsetY: 0
    },
    plotOptions: {
      radialBar: {
        startAngle: -90,
        endAngle: 90,
        dataLabels: {
          name: {
            fontSize: "12px",
            color: undefined,
            offsetY: 10
          },
          value: {
            offsetY: -20,
            fontSize: "12px",
            color: undefined,
            formatter: function(val) {
              return val + "%";
            }
          }
        }
      }
    },
    fill: {
      type: "gradient",
      
      gradient: {
        shade: "dark",
        shadeIntensity: 0.15,
        inverseColors: false,
        opacityFrom: 1,
        opacityTo: 1,
       
        stops: [0, 50, 65, 91]
      }
    },
    stroke: {
      dashArray: 4
    },
    labels: [""]
  };


  public chartOptions2: any = {
    series: [
      {
        name: "Objectifs",
        data: [0,0,0,0,0,0,0,0,0,0,0,0]
      },
      {
        name: "Prévisions",
        data: [0,0,0,0,0,0,0,0,0,0,0,0]
      },
      {
        name: "Réalisations",
        data: [0,0,0,0,0,0,0,0,0,0,0,0]
      }
    ],
    chart: {
      height: 350,
      type: "line",
      zoom: {
        enabled: true
      }
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: "straight"
    },
    title: {
      text: "Variation Objectifs, Prévisions et Réalisations",
      align: "left"
    },
    grid: {
      row: {
        colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
        opacity: 0.5
      }
    },
    xaxis: {
      categories: [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
      ]
    }
  };
  public chartOptions3: any = {
    series: [
      {
        name: "Achats",
        data: [0,0,0,0,0,0,0,0,0,0,0,0]
      },
      {
        name: "Stocks",
        data: [0,0,0,0,0,0,0,0,0,0,0,0]
      },
    ],
    chart: {
      height: 350,
      type: "line",
      zoom: {
        enabled: false
      }
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: "straight"
    },
    title: {
      text: "Variation Achats et Stocks",
      align: "left"
    },
    grid: {
      row: {
        colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
        opacity: 0.5
      }
    },
    xaxis: {
      categories: [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
      ]
    }
  };


  edit(){
    this.chartOptions2.series[0].data[5] = 100
  }


  public chartArea: Object = {
    border: {
        width: 0
    }
};




public load(args: ILoadedEventArgs): void {
  let selectedTheme: string = location.hash.split('/')[1];
  selectedTheme = selectedTheme ? selectedTheme : 'Material';
  args.chart.theme = <ChartTheme>(selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1)).replace(/-dark/i, "Dark");
};
// custom code end
public title: string = 'Graphe Objectifs, Prévisions et Réalisations par famille';
public titlestock: string = 'Graphe Achats et Stocks par famille';
  public width='10%';
    public data: Object[] = [
        { x: 'Egg', y: 2.2 }, { x: 'Fish', y: 2.4 },
        { x: 'Misc', y: 3 }, { x: 'Tea', y: 3.1 }
    ];
    public data1: Object[] = [
        { x: 'Egg', y: 1.2 }, { x: 'Fish', y: 1.3 },
        { x: 'Misc', y: 1.5 }, { x: 'Tea', y: 2.2 }
    ];
    //Initializing Marker
    public marker: Object = {
        dataLabel: {
            visible: true,
            position: 'Top',
            font: {
                fontWeight: '600', color: '#ffffff'
            }
        }
    }
    //Initializing Primary X Axis
    public primaryXAxis: Object = {
        valueType: 'Category',
        title: '',
        interval: 1,
        majorGridLines: { width: 0 }
    };
    //Initializing Primary Y Axis
    public primaryYAxis: Object = {
        labelFormat: '{value}',
        edgeLabelPlacement: 'Shift',
        majorGridLines: { width: 0 },
        majorTickLines: { width: 0 },
        lineStyle: { width: 0 },
        labelStyle: {
            color: 'transparent'
        }
    };
    public tooltip: Object = {
        enable: true
    };


defaultData(){
  var d = new Date()
  var obDate = d.getFullYear()+'-'+(d.getMonth()+1)+'-15'
  return obDate
}

dateMix(){
  var d = new Date()
  var debut
  var fin
  var s = this.selectedOb-5
  if(s<0){
    debut = (this.yearDate()-1)+'-'+(12+s)+'-01'
    if(this.selectedOb == 0){
      fin = (this.yearDate()-1)+'-12-31'
    }else{
      fin = this.yearDate()+'-'+this.selectedOb+'-31'
    }
  }else if(s == 0){
    debut = (this.yearDate()-1)+'-12-01'
    fin = this.yearDate()+'-'+this.selectedOb+'-31'
  }
  else{
    debut = this.yearDate()+'-'+(s)+'-01'
    fin = this.yearDate()+'-'+this.selectedOb+'-31'
  }
  return {
    debut:debut,
    fin:fin
  }
}

dateMix2(){
  var d = new Date()
  var de = (this.yearDate()-1)+'-01-01'
  var fin = (this.yearDate()-1)+'-12-31'

  
  return {
    debut:de,
    fin:fin
  }
  
}

defaultSelectedOb(){
  var d = new Date()
  return d.getMonth()
}
test = false
selectedMonth = this.sd()
number = 1300000
year: any = 0
objectifs = [150,100,120,140,250,320,256,541]

sd(){
  var d = new Date()
  var m = (d.getMonth()+1)>9?(d.getMonth()+1):('0'+(d.getMonth()+1))
  return d.getFullYear()+'-'+m
}


rangeObjectif: any = [0,0,0,0,0,0,0,0,0,0,0,0]
rangePrevision: any = [0,0,0,0,0,0,0,0,0,0,0,0]
rangeRealisation: any = [0,0,0,0,0,0,0,0,0,0,0,0]
rangeRealisationChart: any = [0,0,0,0,0,0,0,0,0,0,0,0]

rangeAchat: any = [0,0,0,0,0,0,0,0,0,0,0,0]
rangeStock: any = [0,0,0,0,0,0,0,0,0,0,0,0]
objectifTotal = 0

getObjectifsRange(){
  this.rangeObjectif = [0,0,0,0,0,0,0,0,0,0,0,0]
  this.rangePrevision = [0,0,0,0,0,0,0,0,0,0,0,0]
  this.rangeAchat = [0,0,0,0,0,0,0,0,0,0,0,0]
  this.rangeStock = [0,0,0,0,0,0,0,0,0,0,0,0]
  this.rangeRealisation = ['...','...','...','...','...','...','...','...','...','...','...','...']
  this.rangeRealisationChart = [0,0,0,0,0,0,0,0,0,0,0,0]
  let postData = new FormData();
  var d= new Date()
  var s = new Date()
  s.setDate(d.getDate()-5)
  var dateStock = s.getFullYear()+'-'+(s.getMonth()+1)+'-'+s.getDay()
  console.log('la dte :: ',dateStock)
  var debut = this.yearDate()+"-01-01"
  var fin = this.yearDate()+"-12-31"
  
  postData.append('dateDebut',debut);
  postData.append('dateFin',fin);
  postData.append('dateDebutc',this.dateMix2().debut);
  postData.append('dateFinc',this.dateMix2().fin);
  this.http.post<any>('https://8000-sapphire-caterpillar-laqzwe8r.ws-eu18.gitpod.io/api/bi/table',postData, this.httpOptions).map(res => res).subscribe(data => {
   console.log(data)
   if(data.achatsM.length>0){
    var prixM = (data.prixc[0].PrixMoy==null || data.prixc[0].PrixMoy == 0)?parseFloat(data.prixe[0].PrixMoy):parseFloat(data.prixc[0].PrixMoy)
     for(var i =0;i<data.achatsM.length;i++){
        this.rangeAchat[(data.achatsM[i].Mois-1)] = parseFloat((parseFloat(data.achatsM[i].prixm)).toFixed(2))
      }
      this.chartOptions3.series[0]['data'] = this.rangeAchat
   }
   if(data.stocksM.length>0){
    
     for(var i =0;i<data.stocksM.length;i++){
        this.rangeStock[(data.stocksM[i].Mois-1)] = parseFloat((parseFloat(data.stocksM[i].prixm).toFixed(2)))
      }
      this.chartOptions3.series[1]['data'] = this.rangeStock
   }
   if(data.objectifs.length>0){
     for(var i =0;i<data.objectifs.length;i++){
        this.rangeObjectif[(data.objectifs[i].Mois-1)] = parseFloat(data.objectifs[i].objectifSum)
      }
      this.chartOptions2.series[0]['data'] = this.rangeObjectif
   }
   if(data.commandes.length>0){
     for(var i =0;i<data.commandes.length;i++){
        this.rangeRealisation[(data.commandes[i].Mois-1)] = parseFloat(data.commandes[i].Sumventes)
        this.rangeRealisationChart[(data.commandes[i].Mois-1)] = parseFloat(data.commandes[i].Sumventes)
     }
     this.chartOptions2.series[2].data = this.rangeRealisationChart
   }
   if(data.previsionsM.length>0){
     var prixM = (data.prixc[0].PrixMoy==null || data.prixc[0].PrixMoy==0)?parseFloat(data.prixe[0].PrixMoy):parseFloat(data.prixc[0].PrixMoy)
     for(var i =0;i<data.previsionsM.length;i++){
        this.rangePrevision[(data.previsionsM[i].Mois-1)] = parseFloat((parseFloat(data.previsionsM[i].prixm)).toFixed(2))
     }
     this.chartOptions2.series[1].data = this.rangePrevision
   }

   if(this.vente){
    this.charttable.updateSeries([
      {
        name: "Objectifs",
        data: this.rangeObjectif
      },
      {
        name: "Prévisions",
        data: this.rangePrevision
      },
      {
        name: "Réalisations",
        data: this.rangeRealisationChart
      }
    ])
   }else{
    this.charttabla.updateSeries([
      {
        name: "Achats",
        data: this.rangeAchat
      },
      {
        name: "Stocks",
        data: this.rangeStock
      },
    ])
   }

   

   


  }, err => {
    console.log(JSON.stringify(err));
  });
}

arraySum(x){
  if(x.length>0){
    const reducer = (a, b) => a.objectif + b.objectif;
    console.log('reducer :: ',reducer)
    return x.reduce(reducer)
  }else{
    return 0
  }
}



dataDetails: any
totalDetails: any = 0
finalData: any
finalDataChart: any
totalPrevisionFamille: any
totalObjectifFamille: any
totalRealisationFamille: any
totalStockFamille: any
totalAchatsFamille: any
getObjectifs(x){
  this.dataDetails = []
  this.finalData = []
  this.finalDataChart = []
  this.objectifTotal = 0
  this.totalObjectifFamille = 0
  this.totalPrevisionFamille = 0
  this.totalRealisationFamille = 0
  this.totalStockFamille = 0
  this.totalAchatsFamille = 0
  let postData = new FormData();
  var d= new Date()
  var s = new Date()
  s.setDate(d.getDate()-5)
  var dateStock = s.getFullYear()+'-'+(s.getMonth()+1)+'-'+s.getDay()
  console.log('la dte :: ',dateStock)
  var debut = this.yearDate()+'-'+(this.selectedOb+1)+"-01"
  var fin = this.yearDate()+'-'+(this.selectedOb+1)+"-31"
  
  postData.append('dateDebut',debut);
  postData.append('dateFin',fin);
  postData.append('dateDebutMix',this.dateMix().debut);
  postData.append('dateFinMix',this.dateMix().fin);
  postData.append('dateDebutc',this.dateMix2().debut);
  postData.append('dateFinc',this.dateMix2().fin);
  postData.append('dateObjectif',x);
  this.http.post<any>('https://8000-sapphire-caterpillar-laqzwe8r.ws-eu18.gitpod.io/api/bi/table/details',postData, this.httpOptions).map(res => res).subscribe(data => {
   console.log(data)
   var commandes = data.commandes
   var objectifs = data.objectifs
   var familles = data.familles
   var mix = data.mix
   var prevision = data.previsionsM
   var achats = data.achatsM
   var stocksM = data.stocksM
   var prixMoy = data.prixe
   var prixMoyc = data.prixc
   var prixMoyGlob = (data.prixSum[0]?.Sumventes || 0)
   var mixTotal = parseFloat(data.mixTotal[0].Sumventes)
   var finalData = []
   var finalDataChart = []
   var sumP
   var sumO
   var sumR
  this.objectifTotal = this.arraySum(objectifs)
   for(var i = 0;i<familles.length;i++){
     var cmd = commandes.filter(({famille_id})=>famille_id === familles[i].id)
     var ob = objectifs.filter(({famille_id})=>famille_id === familles[i].id)
     var m = mix.filter(({famille_id})=>famille_id === familles[i].id)
     var p = prevision.filter(({famille_id})=>famille_id === familles[i].id)
     var s = stocksM.filter(({famille_id})=>famille_id === familles[i].id)
     var a = achats.filter(({famille_id})=>famille_id === familles[i].id)
     var p1 = prixMoy.filter(({famille_id})=>famille_id === familles[i].id)
     var p2 = prixMoyc.filter(({famille_id})=>famille_id === familles[i].id)
     var x
     var y
      var prixMoyp = (prixMoyGlob==null || prixMoyGlob==0)?parseFloat(p1[0]?.PrixMoy):parseFloat(p2[0]?.PrixMoy)
      console.log('prixMoyp :: ',prixMoyp)
     this.totalObjectifFamille = this.totalObjectifFamille + (ob.length>0?parseFloat(ob[0].objectif):0)
     this.totalPrevisionFamille = this.totalPrevisionFamille + (p.length>0?parseFloat(p[0].prixm):0)
     this.totalRealisationFamille = this.totalRealisationFamille + (cmd.length>0?parseFloat(cmd[0].Sumventes):0)
     this.totalStockFamille = this.totalStockFamille + (s.length>0?(parseFloat(s[0].prixm)):0)
     this.totalAchatsFamille = this.totalAchatsFamille + (a.length>0?(parseFloat(a[0].prixm)):0)
     x = {
        famille_id:familles[i].id,
        famille:familles[i].famille,
        objectif:ob.length>0?parseFloat(ob[0].objectif):'....',
        realisation:cmd.length>0?parseFloat(cmd[0].Sumventes):'....',
        mix:(100*parseFloat(m[0].Sumventes)/mixTotal).toFixed(2),
        prevision:p.length>0?parseFloat((parseFloat(p[0].prixm)).toFixed(2)):0,
        stock:s.length>0?parseFloat((parseFloat(s[0].prixm)).toFixed(2)):0,
        achat:a.length>0?parseFloat((parseFloat(a[0].prixm).toFixed(2))):0
      }
    y = {
        famille:familles[i].famille,
        objectif:ob.length>0?ob[0].objectif:0,
        realisation:cmd.length>0?parseFloat(cmd[0].Sumventes):0,
        prevision:p.length>0?parseFloat((parseFloat(p[0].prixm)).toFixed(2)):0,
        stock:s.length>0?parseFloat((parseFloat(s[0].prixm)).toFixed(2)):0,
        achat:a.length>0?parseFloat((parseFloat(a[0].prixm)).toFixed(2)):0
      }
    finalData.push(x)
    finalDataChart.push(y)

   }

   this.finalData = finalData
   this.finalDataChart = finalDataChart
   console.log('chart',this.finalData)

  //  this.dataDetails = data
  //  this.totalDetails = 0
  //  if(data.length>0){
  //    for(var i=0;i<data.length;i++){
  //      this.totalDetails = this.totalDetails + data[i].objectif
  //    }
  //  }
  }, err => {
    console.log(JSON.stringify(err));
  });
}

percentCalc(obj){
  var res = (obj/this.totalDetails)*100
  return res.toFixed(2)
}

dateRange(x){
  var d = new Date()
  var dates = []
  for(var i =1 ;i<=12;i++){
    if(i<10){
      var s = (d.getFullYear()+x)+'-0'+i
      dates.push(s)
    }else{
      var s = (d.getFullYear()+x)+'-'+i
      dates.push(s)
    }
  }
  return dates
}

yearDate(){
  var d = new Date()
  var year = this.year + d.getFullYear()
  return year
}

nextYear(){
  this.year = this.year+1
  this.getObjectifsRange()
}
previousYear(){
  this.year = this.year-1
  this.getObjectifsRange()
}

CurrencyFormat(number)
{
   return number.toLocaleString()
}

selectedOb = this.defaultSelectedOb()
selectObjectif(i){
  this.test = true
  this.selectedOb = i
  this.selectedMonth = this.dateRange(this.year)[i]
  console.log('index ',i)
  console.log('date selected : ',this.dateRange(this.year)[i])
  var c = this.selectedMonth+'-15'
  this.getObjectifs(c)
  this.showInputIndex = 100
}

showInputIndex = 100
showInputObGlobal(index,value){
  var d = new Date()
  var m = d.getMonth()
  if(index>= m && this.selectedOb == index){
    this.showInputIndex = index
    this.globalInputValue = value
  }
  
}

globalInputValue: any
confirmOb(){
  var data = []
  console.log(this.globalInputValue)
  var c = this.selectedMonth+'-15'
  this.saveObjectif(c,this.globalInputValue)
  console.log('data MIXED ',data)
}



saveObjectif(x,objectif){
  this.dataDetails = []
  this.finalData = []
  this.finalDataChart = []
  let postData = new FormData();
  var d= new Date()
  var s = new Date()
  s.setDate(d.getDate()-5)
  var dateStock = s.getFullYear()+'-'+(s.getMonth()+1)+'-'+s.getDay()
  console.log('la dte :: ',dateStock)
  var debut = this.yearDate()+'-'+(this.selectedOb+1)+"-01"
  var fin = this.yearDate()+'-'+(this.selectedOb+1)+"-31"
  
  postData.append('dateDebut',debut);
  postData.append('dateFin',fin);
  postData.append('dateDebutMix',this.dateMix().debut);
  postData.append('dateFinMix',this.dateMix().fin);
  postData.append('dateObjectif',x);
  postData.append('objectif',objectif);
  this.http.post<any>('https://8000-sapphire-caterpillar-laqzwe8r.ws-eu18.gitpod.io/api/objectifs/add',postData, this.httpOptions).map(res => res).subscribe(data => {
   console.log('edition ou ajout done')
   this.rangeObjectif[this.selectedOb]= objectif
   this.showInputIndex = 100
   this.getObjectifs(x)
  }, err => {
    console.log(JSON.stringify(err));
  });
}


//BI EDITION HERE
// FIRST CHART

public width1 = '100%';
 // custom code end
 public john: Object[] = [
  { x: new Date(2000, 2, 11), y: 15 }, { x: new Date(2000, 9, 14), y: 20 },
  { x: new Date(2001, 2, 11), y: 25 }, { x: new Date(2001, 9, 16), y: 21 },
  { x: new Date(2002, 2, 7), y: 13 }, { x: new Date(2002, 9, 7), y: 18 },
  { x: new Date(2003, 2, 11), y: 24 }, { x: new Date(2003, 9, 14), y: 23 },
  { x: new Date(2004, 2, 6), y: 19 }, { x: new Date(2004, 9, 6), y: 31 },
  { x: new Date(2005, 2, 11), y: 39 }, { x: new Date(2005, 9, 11), y: 50 },
  { x: new Date(2006, 2, 11), y: 24 }
];
public andrew: Object[] = [
  { x: new Date(2000, 2, 11), y: 39 }, { x: new Date(2000, 9, 14), y: 30 },
  { x: new Date(2001, 2, 11), y: 28 }, { x: new Date(2001, 9, 16), y: 35 },
  { x: new Date(2002, 2, 7), y: 39 }, { x: new Date(2002, 9, 7), y: 41 },
  { x: new Date(2003, 2, 11), y: 45 }, { x: new Date(2003, 9, 14), y: 48 },
  { x: new Date(2004, 2, 6), y: 54 }, { x: new Date(2004, 9, 6), y: 55 },
  { x: new Date(2005, 2, 11), y: 57 }, { x: new Date(2005, 9, 11), y: 60 },
  { x: new Date(2006, 2, 11), y: 60 }
];
public thomas: Object[] = [
  { x: new Date(2000, 2, 11), y: 60 }, { x: new Date(2000, 9, 14), y: 55 },
  { x: new Date(2001, 2, 11), y: 48 }, { x: new Date(2001, 9, 16), y: 57 },
  { x: new Date(2002, 2, 7), y: 62 }, { x: new Date(2002, 9, 7), y: 64 },
  { x: new Date(2003, 2, 11), y: 57 }, { x: new Date(2003, 9, 14), y: 53 },
  { x: new Date(2004, 2, 6), y: 63 }, { x: new Date(2004, 9, 6), y: 50 },
  { x: new Date(2005, 2, 11), y: 66 }, { x: new Date(2005, 9, 11), y: 65 },
  { x: new Date(2006, 2, 11), y: 79 }
];
//Initializing Primary X Axis
public primaryXAxis1: Object = {
  inimum: new Date(2000, 1, 1), maximum: new Date(2006, 2, 11),
  valueType: 'DateTime',
  skeleton: 'y',
  lineStyle: { width: 0 },
  majorGridLines: { width: 0 },
  edgeLabelPlacement: 'Shift'
};
//Initializing Primary Y Axis
public primaryYAxis1: Object = {
  title: 'Revenue',
  labelFormat: '{value}M',
  majorTickLines: { width: 0 },
  minimum: 10, maximum: 80,
  lineStyle: { width: 0 },
};
public chartArea1: Object = {
  border: {
      width: 0
  }
};
// custom code start
// custom code end
public title1: string = 'Average Sales per Person';
public marker1: Object = { visible: true };
public tooltip1: Object = { enable: true, shared: true };
public crosshair1: Object = { enable: true, lineType: 'Vertical' };

public columnData: object[] = [
  { x: 1, xval: 'Jan', yval:0 },
  { x: 2, xval: 'Feb', yval:0 },
  { x: 3, xval: 'Mar', yval:0 },
  { x: 4, xval: 'Apr', yval: 0 },
  { x: 5, xval: 'May', yval: 0 },
  { x: 6, xval: 'Jun', yval:0 },
  { x: 7, xval: 'Jul', yval: 0 },
  { x: 8, xval: 'Aug', yval:0 },
  { x: 9, xval: 'Sep', yval: 0 },
  { x: 10, xval: 'Oct', yval:0 },
  { x: 11, xval: 'Nov', yval:0 },
  { x: 12, xval: 'Dec', yval:0 },
];
public columntooltipSettings: object = {
  visible: true,
  format: '${xval} : ${yval}',
};

statsGlobal: any
getStatsGlobal(){
  let postData = new FormData()
  postData.append('test','1');
  var debut = this.yearDate()+"-01-01"
  var fin = this.yearDate()+"-12-31"
  
  postData.append('dateDebut',debut);
  postData.append('dateFin',fin);
  postData.append('dateDebutc',this.dateMix2().debut);
  postData.append('dateFinc',this.dateMix2().fin);
  this.http.post<any>('https://8000-sapphire-caterpillar-laqzwe8r.ws-eu18.gitpod.io/api/bi/stats',postData, this.httpOptions).map(res => res).subscribe(data => {
    this.statsGlobal = data
    var date = new Date()
    var m = date.getMonth()+1
    var n = date.getMonth()
    var y = date.getFullYear()
    var stockNow = 0
    var stockPrev = 0
    if(n>0){
      var castock = data.stocksM.filter(({Mois,Year})=>Mois === m && Year === y)
      var castockprev = data.stocksM.filter(({Mois,Year})=>Mois === n && Year === y)
      stockNow = castock.lengh>0?castock[0].prixm:0
      stockPrev = castockprev.length>0?castockprev[0].prixm:0
    }else{
      var castock = data.stocksM.filter(({Mois,Year})=>Mois === m && Year === y)
      var castockprev = data.stocksM.filter(({Mois,Year})=>Mois === 12 && Year === (y-1))
      stockNow = castock.lengh>0?castock[0].prixm:0
      stockPrev = castockprev.length>0?castockprev[0].prixm:0
    }

    if(n>0){
      for(var i =0;i<12;i++){
        var d = data.stocks.filter(({Mois,Year})=>Mois === (i+1) && Year === y)
        if(d.length>0){
          this.columnData[i]['yval'] = d[0].qte
        }else{
          this.columnData[i]['yval'] = 0
        }
      }
    }else{
      for(var i =0;i<12;i++){
        var d = data.stocks.filter(({Mois,Year})=>Mois === (i+1) && Year === y)
       
        if(d.length>0){
          this.columnData[i]['yval'] = d[0].qte
         
        }else{
          this.columnData[i]['yval'] = 0
        }
        
      }
    }
   var f = new Date()
   var mo: any = (f.getMonth()+1)+'-'+f.getFullYear()
    var stats = {
      date1:data?.ventesDernier[0]?.Mois+'-'+data?.ventesDernier[0]?.Year,
      date2:mo,
      ventes1:parseFloat(data.ventesDernier[0].Sumventes).toLocaleString(),
      ventes2:parseFloat(data.ventesEncours[0]?.Sumventes || 0).toLocaleString(),
      dif:(100*(parseFloat(data.ventesEncours[0]?.Sumventes || 0)-parseFloat(data.ventesDernier[0]?.Sumventes || 0))/parseFloat(data.ventesDernier[0]?.Sumventes || 0)).toFixed(2),
      negative:(parseFloat(data.ventesEncours[0].Sumventes)-parseFloat(data.ventesDernier[0].Sumventes))>0?false:true,
      objectif1:parseFloat(data.objectifDernier[0].objectifSum).toLocaleString(),
      objectif2:parseFloat(data.objectifEncours[0].objectifSum).toLocaleString(),
      objectifPer:(100*(parseFloat(data.ventesEncours[0]?.Sumventes || 0)/parseFloat(data.objectifEncours[0].objectifSum))),
      achatsNextDate:data.achatsSuivant[0].Mois+'-'+data.achatsSuivant[0].Year,
      achatsNextQte:data.achatsSuivant[0].qte,
      achatsNextNbre:data.achatsSuivant[0].nbre,
      achatsQte:data.achatsEncours[0].qte,
      achatsNbre:data.achatsEncours[0].nbre,
      stocknow:stockNow,
      stockprev:stockPrev,
      dateStock:n>0?(n+'-'+y):(12+'-'+(y-1)),
      prixMoy:data.prixMoyc[0].PrixMoy==null?parseFloat(data.prixMoye[0].PrixMoy):parseFloat(data.prixMoyc[0].PrixMoy),
      stockCA:this.stockCA(stockNow,((data.prixMoyc[0].PrixMoy==null || data.prixMoyc[0].PrixMoy==0)?parseFloat(data.prixMoye[0].PrixMoy):parseFloat(data.prixMoyc[0].PrixMoy))),
      stockCAprev:this.stockCA(stockPrev,((data.prixMoyc[0].PrixMoy==null || data.prixMoyc[0].PrixMoy==0)?parseFloat(data.prixMoye[0].PrixMoy):parseFloat(data.prixMoyc[0].PrixMoy))),
      couverture:this.couverture(data.ventesEncours[0].Sumventes,stockPrev),
    }
    this.statsGlobal = stats
    this.chartOptions.series[0] = Math.round(stats.objectifPer)
    console.log('the glova',this.statsGlobal)
  }, err => {
    console.log(JSON.stringify(err));
  });
}
vente = true

stockCA(stock,prixm){
  var s = (parseFloat(stock)*parseFloat(prixm)).toFixed(2)
  return parseFloat(s)
}
couverture(c,stock){
  var vmj: any = parseFloat(c)/22
  var stock: any = parseFloat(stock)
  var s: any = (parseFloat(stock)/parseFloat(vmj)).toFixed(2)
  return parseFloat(s)
}
}
