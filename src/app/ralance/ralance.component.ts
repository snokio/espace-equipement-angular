import { Component, OnInit,ViewChild} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SortService } from '@syncfusion/ej2-angular-grids';
import { GridComponent,ToolbarItems,PageSettingsModel } from '@syncfusion/ej2-angular-grids';
import { ClickEventArgs } from '@syncfusion/ej2-navigations'
import { numberFormatting } from '@syncfusion/ej2-angular-pivotview';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-ralance',
  templateUrl: './ralance.component.html',
  styleUrls: ['./ralance.component.scss'],
  providers: [SortService]
})
export class RalanceComponent implements OnInit {
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + window.localStorage.getItem('ESP_access_token')
    })
  }
  constructor(private http: HttpClient) { }
  public initialPage: Object;
  public initialSort: Object;

  toolbar: ToolbarItems[] | object;
 
  @ViewChild('grid')
  public grid: GridComponent;

  ngOnInit() {
    this.toolbar = [
      'CsvExport',
      'ExcelExport',
      'Print',
      'Search'
    ];
    
    
    this.getFamilleListe()
    this.initialPage = { pageSizes: true, pageCount: 1 };
    this.initialSort = {
      columns: [{ field: 'cmder', direction: 'Ascending' },
    ]
  };

  this.getArticleListe('')
  }

  toolbarClick(args: ClickEventArgs): void {
    switch (args.item.text) {
        case 'PDF Export':
            this.grid.pdfExport();
            break;
        case 'Excel Export':
           this.exportAsExcelFile(this.dataGlobal,'Relances')
            // this.grid.excelExport();
            break;
        case 'CSV Export':
            this.grid.csvExport();
            break;
    }
   } 
  FamilleListe: any
  familleD: any
  getFamilleListe(){
    this.http.get<any>('https://8000-sapphire-caterpillar-laqzwe8r.ws-eu18.gitpod.io/api/params', this.httpOptions).map(res => res).subscribe(data => {
      this.FamilleListe = data.familles
    }, err => {
      console.log(JSON.stringify(err));
    });
  }

  ArticleListe: any
  selectedParamArticle: any
  stockInit: any
  lot: any
  delai: any
  dtf: any
  dataLoading = false
  dataGlobal: any
  getArticleListe(id){
    this.dataGlobal = []
    let postData = new FormData();
    var d= new Date()
    var s = new Date()
    s.setDate(d.getDate()-5)
    var dateStock = s.getFullYear()+'-'+(s.getMonth()+1)+'-'+s.getDay()
    console.log('la dte :: ',dateStock)
    var debut = d.getFullYear()+'-'+(d.getMonth()+1)+"-01"
    var fin = d.getFullYear()+'-'+(d.getMonth()+1)+"-31"
    postData.append('dateDebut',debut);
    postData.append('dateFin',fin);
    postData.append('datestock',dateStock);
    postData.append('familleID', id);
    this.dataLoading = true
    this.http.post<any>('https://8000-sapphire-caterpillar-laqzwe8r.ws-eu18.gitpod.io/api/ralance/articles',postData, this.httpOptions).map(res => res).subscribe(data => {
      this.dataLoading = false
      this.ArticleListe = data

      for(var i =0;i<data.length;i++){
        var x = {
          famille:data[i].famille.famille,
          code:data[i].code,
          designation:data[i].designation,
          classe:data[i].classe,
          cmd:0,
          vmj:0,
          stock:0,
          couverture:0
        }
        if(data[i].commandes_m.length>0){
          x.cmd = Number(data[i].commandes_m[0].Sumventes)
          x.vmj = Number(this.vmj(data[i].commandes_m[0].Sumventes))
        }

        if(data[i].stocks_m.length>0){
          x.stock = Number(data[i].stocks_m[0].Sumstock)
         
        }

        if(data[i].commandes_m.length>0){
          x.couverture= this.couverture(x.stock,data[i].commandes_m[0].Sumventes) as number
        }

       this.dataGlobal.push(x)

      }
      this.grid.refresh();

      console.log('data here ',this.dataGlobal)
     
    }, err => {
      console.log(JSON.stringify(err));
      this.dataLoading = false
    });
  }
  selectFamille(id) {
    // console.log(id);
    this.familleD = id
    this.ArticleListe = []
    this.selectedParamArticle = ''
    this.getArticleListe(id)
}

selectArticle(id) {
  this.selectedParamArticle = id
}

vmj(a){
  var cmd = parseFloat(a)
  var x = cmd/22
  return x.toFixed(2)
}

couverture(stock,commande){
  var vmj = this.vmj(commande)
  if(parseFloat(vmj)>0){
    var c = stock/parseFloat(vmj)
    return c.toFixed(2)
  }else{
    return 0
  }
}

toExportFileName(excelFileName: string): string {
  return `${excelFileName}_export_${new Date().getTime()}.xlsx`;
}

public exportAsExcelFile(json: any[], excelFileName: string): void {
  const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
  const workbook: XLSX.WorkBook = {Sheets: {'data': worksheet}, SheetNames: ['data']};
  XLSX.writeFile(workbook, this.toExportFileName(excelFileName));
}

}
