 //SECOND TEST
 transformPrevisionDataMoisOrigin(data,params) {
    var itemPrev = data.previsions_s
    var itemCmd = data.commandes_m
    var itemStock = data.stocks_m
    var itemCmdClient = data.cmdplanifie
    var itemAchat = data.achatplanifie
    var itemStockinit = data.stockone
    var itemPrev2 = data.previsions_t
    var itemCmd2 = data.commandes_x
    var itemTauxSecurite = data.tau.facteur_z

    var delai
    if(params == null){
      // delai = this.delaiG
      delai = 2
    }else{
      delai = params.delai
    }
    var infos = data
    //Prevision Part here
  if (itemPrev.length > 0 ) {
    if (itemPrev.length > 0 ) {
      for (var i = 0; i < itemPrev.length; i++) {
        var mois: any = 0
        var prevision
        var cmdPlanifie
        var moisType

        if(itemPrev[i].Mois >= 10){
          moisType = itemPrev[i].Mois + '-' + itemPrev[i].Year
        }else{
          moisType =  '0'+itemPrev[i].Mois + '-' + itemPrev[i].Year
        }

        prevision = {
          'ID': infos.code,
          'Amount': itemPrev[i].Sumprev,
          'Article': infos.designation,
          'TypeParams': '1- Prévision',
          'Year': itemPrev[i].Year,
          'Month':  moisType,
          'Mois':itemPrev[i].Mois,
        }
        
      
        this.newData.push(prevision)
      
        
      }
    }


    if(itemCmdClient.length>0){
      var d = new Date()
      var month = d.getMonth()+1
      var year = d.getFullYear()
      var date 
      if(month<10){
        date = '0'+month+'-'+year
      }else{
        date = month+'-'+year
      }
      // console.log(date)
      if(this.newData.length > 0){
        const result = this.newData.filter(({TypeParams,ID})=>TypeParams==='1- Prévision' && ID === infos.code) 
        for(var k =0;k<12;k++){
          //  var cm = itemStock.filter(({Mois,Year})=> Mois === result[k].Mois && Year === result[k].Year )
        var cm = itemCmdClient.filter(({Mois,Year})=> Mois === result[k].Mois && Year === result[k].Year )
          if(cm.length > 0){
            if(cm.Mois>10){
              moisType = cm[0].Mois + '-' + cm[0].Year
            }else{
              moisType = '0'+cm[0].Mois + '-' + cm[0].Year
            }
            var cmdo: any = {
              'ID': infos.code,
              'Amount': cm[0].cmds,
              'Article': infos.designation,
              'TypeParams': '3- Commande Client',
              'Year': cm[0].Year,
              'Month':  moisType,
            }
            this.newData.push(cmdo)
            
          }else{
            if(result[k].Mois>10){
              moisType = result[k].Mois + '-' + result[k].Year
            }else{
            moisType = '0'+result[k].Mois + '-' + result[k].Year
            }
            var cmf: any = {
              'ID': infos.code,
              'Amount':0,
              'Article': infos.designation,
              'TypeParams': '3- Commande Client',
              'Year': result[k].Year,
              'Month':  moisType,
              // 'Semaine': 'Semaine '+result[k].Week
            }
            this.newData.push(cmf)
          }
        }
      }
      
    }else{
      const result = this.newData.filter(({TypeParams,ID})=>TypeParams==='1- Prévision' && ID === infos.code) 
      console.log(result)
      if(result.length==12){
        for(var k =0;k<12;k++){
          //  var cm = itemStock.filter(({Mois,Year})=> Mois === result[k].Mois && Year === result[k].Year )
          if(result[k].Mois>10){
            moisType = result[k].Mois + '-' + result[k].Year
          }else{
          moisType = '0'+result[k].Mois + '-' + result[k].Year
          }
          var cmdClientplanifie = {
            'ID': infos.code,
            'Amount': 0,
            'Article': infos.designation,
            'TypeParams': '3- Commande Client',
            'Year': result[k].Year,
            'Month':  moisType,
          }
          this.newData.push(cmdClientplanifie)
        }
      }
      
      
    
      
    }


    if(itemAchat.length>0){
      var d = new Date()
      var month = d.getMonth()+1
      var year = d.getFullYear()
      var date 
      if(month<10){
        date = '0'+month+'-'+year
      }else{
        date = month+'-'+year
      } 

      // console.log(date)
      if(this.newData.length > 0){
        const result = this.newData.filter(({TypeParams,ID})=>TypeParams==='1- Prévision' && ID === infos.code) 
        for(var k =0;k<12;k++){
          //  var cm = itemStock.filter(({Mois,Year})=> Mois === result[k].Mois && Year === result[k].Year )
        var cm = itemAchat.filter(({Mois,Year})=> Mois === result[k].Mois && Year === result[k].Year )
          if(cm.length > 0){
            if(cm[0].Mois>9){
              moisType = cm[0].Mois + '-' + cm[0].Year
            }else{
              moisType = '0'+cm[0].Mois + '-' + cm[0].Year
            }
            var cmdo: any = {
              'ID': infos.code,
              'Amount': cm[0].cmds,
              'Article': infos.designation,
              'TypeParams': '6- Commande Planifiée',
              'Year': cm[0].Year,
              'Month':  moisType,
            }
            this.newData.push(cmdo)
            
          }else{
            if(result[k].Mois>9){
              moisType = result[k].Mois + '-' + result[k].Year
            }else{
            moisType = '0'+result[k].Mois + '-' + result[k].Year
            }
            var cmf: any = {
              'ID': infos.code,
              'Amount':0,
              'Article': infos.designation,
              'TypeParams': '6- Commande Planifiée',
              'Year': result[k].Year,
              'Month':  moisType,
              // 'Semaine': 'Semaine '+result[k].Week
            }
            this.newData.push(cmf)
          }
        }
      }
      
    }else{
      const result = this.newData.filter(({TypeParams,ID})=>TypeParams==='1- Prévision' && ID === infos.code) 
      if(result.length == 12){
        for(var k =0;k<12;k++){
          //  var cm = itemStock.filter(({Mois,Year})=> Mois === result[k].Mois && Year === result[k].Year )
          if(result[k].Mois>9){
            moisType = result[k].Mois + '-' + result[k].Year
          }else{
          moisType = '0'+result[k].Mois + '-' + result[k].Year
          }
          var cmdClientplanifie = {
            'ID': infos.code,
            'Amount': 0,
            'Article': infos.designation,
            'TypeParams': '6- Commande Planifiée',
            'Year': result[k].Year,
            'Month':  moisType,
          }
          this.newData.push(cmdClientplanifie)
        }
        
      }
     
    
      
    }

    // Commande Clients part here
    if(itemCmd.length>0){
      var d = new Date()
      var month = d.getMonth()+1
      var year = d.getFullYear()
      var date 
      var dmdnet
      if(month<10){
        date = '0'+month+'-'+year
      }else{
        date = month+'-'+year
      }
      // console.log(date)
      const result = this.newData.filter(({TypeParams,ID, Month})=>TypeParams==='1- Prévision' && ID === infos.code)
      const allPrev = this.newData.filter(({TypeParams,ID})=>TypeParams==='1- Prévision' && ID === infos.code)
      // console.log('alrs les resu ',result)
      // console.log('all prevision est : ',allPrev)
      for(var k =0;k<12;k++){
        var cm = itemCmd.filter(({Mois,Year})=> Mois === result[k].Mois && Year === result[k].Year )
        
        var moisType
        if(cm.Mois >= 10){
          moisType =  result[k].Mois+ '-' + result[k].Year
        }else{
          moisType =  '0'+  result[k].Mois + '-' + result[k].Year
        }
        if(cm.length >0){
          var cmd = {
            'ID': infos.code,
            'Amount': cm[0].Sumventes,
            'Article': infos.designation,
            'TypeParams': '2- Ventes',
            'Year': cm[0].Year,
            'Month': moisType,
          }
          this.newData.push(cmd) 
          dmdnet = {
            'ID': infos.code,
            'Amount': allPrev[k].Amount-parseFloat(cm[0].Sumventes),
            'Article': infos.designation,
            'TypeParams': '4- Demande Net',
            'Year': cm[0].Year,
            'Month': moisType,
            'Mois':cm[0].Mois,
          }
          this.newData.push(dmdnet) 
        }else{
          var cmf: any = {
            'ID': infos.code,
            'Amount':0,
            'Article': infos.designation,
            'TypeParams': '2- Ventes',
            'Year': result[k].Year,
            'Month': moisType,
          }
          this.newData.push(cmf)
          dmdnet = {
            'ID': infos.code,
            'Amount': allPrev[k].Amount,
            'Article': infos.designation,
            'TypeParams': '4- Demande Net',
            'Year': result[k].Year,
            'Month': moisType,
          }
          this.newData.push(dmdnet)
        }
      }
     
    }else{
      var d = new Date()
      var month = d.getMonth()+1
      var year = d.getFullYear()
      var date 
      var dmdnet
      if(month<10){
        date = '0'+month+'-'+year
      }else{
        date = month+'-'+year
      }
      // console.log(date)
      const result = this.newData.filter(({TypeParams,ID, Month})=>TypeParams==='1- Prévision' && ID === infos.code)
      const allPrev = this.newData.filter(({TypeParams,ID})=>TypeParams==='1- Prévision' && ID === infos.code)
      // console.log('alrs les resu ',result)
      // console.log('all prevision est : ',allPrev)
      if(result.length == 12){
        for(var k =0;k<12;k++){
          var cm = itemCmd.filter(({Mois,Year})=> Mois === result[k].Mois && Year === result[k].Year )
          
          var moisType
          if(cm.Mois >= 10){
            moisType =  result[k].Mois+ '-' + result[k].Year
          }else{
            moisType =  '0'+  result[k].Mois + '-' + result[k].Year
          }
          
            var cmf: any = {
              'ID': infos.code,
              'Amount':0,
              'Article': infos.designation,
              'TypeParams': '2- Ventes',
              'Year': result[k].Year,
              'Month': moisType,
            }
            this.newData.push(cmf)
            dmdnet = {
              'ID': infos.code,
              'Amount': allPrev[k].Amount,
              'Article': infos.designation,
              'TypeParams': '4- Demande Net',
              'Year': result[k].Year,
              'Month': moisType,
            }
            this.newData.push(dmdnet)
          
        }
      }
      
    }

    
    //Fonction de la demande 
    if(itemPrev.length>0){
      var previsions = this.newData.filter(({TypeParams,ID})=>TypeParams === '1- Prévision' && ID === infos.code)
      var dmdNet = this.newData.filter(({TypeParams,ID})=>TypeParams === '4- Demande Net' && ID === infos.code)
      var commandesClient = this.newData.filter(({TypeParams,ID})=>TypeParams === '3- Commande Client' && ID === infos.code)
      console.log('f',dmdNet)
      if(dmdNet.length != 0 || commandesClient.length != 0){
        for(var i = 0;i<12;i++){
        
            var cmf: any = {
              'ID': infos.code,
              'Amount': Math.max(dmdNet[i].Amount,commandesClient[i].Amount),
              'Article': infos.designation,
              'TypeParams': '5- Demande',
              'Year': previsions[i].Year,
              'Month':  previsions[i].Month,
              // 'Semaine': previsions[i].Semaine
            }
            this.newData.push(cmf)
          
        }
      }
      
    }

    // else if(commandes[i]){
    //   var cmds = parseFloat(commandes[i].Amount)+parseFloat(commandesClient[i].Amount)
    //   console.log('alors on cherche la table CMDS : ',parseFloat(commandes[i].Amount),parseFloat(commandesClient[i].Amount),cmds)
    //   var cmf: any = {
    //     'ID': infos.code,
    //     'Amount': Math.max(previsions[i].Amount,cmds),
    //     'Article': infos.designation,
    //     'TypeParams': '5- Demande',
    //     'Year': previsions[i].Year,
    //     'Month':  previsions[i].Month,
    //     // 'Semaine': previsions[i].Semaine
    //   }
    //   this.newData.push(cmf)
    // }else{
    //   var cmf: any = {
    //     'ID': infos.code,
    //     'Amount': previsions[i].Amount,
    //     'Article': infos.designation,
    //     'TypeParams': '5- Demande',
    //     'Year': previsions[i].Year,
    //     'Month':  previsions[i].Month,
    //   }
    //   this.newData.push(cmf)
    // }

    //Fonction du Besoin Net
    if(itemPrev.length>0){
      var demandes = this.newData.filter(({TypeParams,ID})=>TypeParams === '5- Demande' && ID === infos.code)
      var stocks = this.newData.filter(({TypeParams,ID})=>TypeParams === '7- Stock Réel' && ID === infos.code)
      var cmdplan = this.newData.filter(({TypeParams,ID})=>TypeParams === '6- Commande Planifiée' && ID === infos.code)
      
      var stockinit
      // console.log('la stock intitnti : ',itemStockinit[0])
      if(itemStockinit.length>0){
        stockinit = itemStockinit[0].quantite
      }else{
        stockinit = 0
      }

      for(var i=0;i<12;i++){
        if(i>0){
          var besoin = parseFloat(cmdplan[i].Amount)-parseFloat(demandes[i].Amount)
          // var besoin = parseFloat(stockinit)+parseFloat(cmdplan[i].Amount)-parseFloat(demandes[i].Amount)
          // console.log('le besoin est ded :: ',besoin)
          if(besoin<0){
              var bs = {
                'ID': infos.code,
                'Amount': besoin,
                'Article': infos.designation,
                'TypeParams': '8- Besoin Net',
                'Year': previsions[i].Year,
                'Month':  previsions[i].Month,
              }
              this.newData.push(bs)
              var por = {
                'ID': infos.code,
                'Amount': Math.abs(besoin),
                'Article': infos.designation,
                'TypeParams': '9- Planned Order Receipt',
                'Year': previsions[i].Year,
                'Month':  previsions[i].Month,
              }
              this.newData.push(por)
            }else{
              var bs = {
                'ID': infos.code,
                'Amount': 0,
                'Article': infos.designation,
                'TypeParams': '8- Besoin Net',
                'Year': previsions[i].Year,
                'Month':  previsions[i].Month,
              }
              this.newData.push(bs)
              var por = {
                'ID': infos.code,
                'Amount': 0,
                'Article': infos.designation,
                'TypeParams': '9- Planned Order Receipt',
                'Year': previsions[i].Year,
                'Month':  previsions[i].Month,
              }
              this.newData.push(por)
            }
        }else{
          
          var besoin = parseFloat(stockinit)+parseFloat(cmdplan[i].Amount)-parseFloat(demandes[i].Amount)
          // console.log('le besoin est ded :: ',besoin)
          if(besoin<0){
              var bs = {
                'ID': infos.code,
                'Amount': besoin,
                'Article': infos.designation,
                'TypeParams': '8- Besoin Net',
                'Year': previsions[i].Year,
                'Month':  previsions[i].Month,
              }
              this.newData.push(bs)
              var por = {
                'ID': infos.code,
                'Amount': Math.abs(besoin),
                'Article': infos.designation,
                'TypeParams': '9- Planned Order Receipt',
                'Year': previsions[i].Year,
                'Month':  previsions[i].Month,
              }
              this.newData.push(por)
            }else{
              var bs = {
                'ID': infos.code,
                'Amount': 0,
                'Article': infos.designation,
                'TypeParams': '8- Besoin Net',
                'Year': previsions[i].Year,
                'Month':  previsions[i].Month,
              }
              this.newData.push(bs)
              var por = {
                'ID': infos.code,
                'Amount': 0,
                'Article': infos.designation,
                'TypeParams': '9- Planned Order Receipt',
                'Year': previsions[i].Year,
                'Month':  previsions[i].Month,
              }
              this.newData.push(por)
            }
        }
      }

      for(var g=0 ;g<this.listeConfirmation.length;g++){
        var dd = this.listeConfirmation[g]
        var ndxc = this.newData.findIndex(({TypeParams,ID,Month})=>TypeParams==='9- Planned Order Receipt' && ID === dd.article_code && Month===dd.month_rec)
        if(ndxc>0){
          this.newData[ndxc].Amount = dd.po_release
        }
      }
      
     
      //Planner Order Realease
      var pors = this.newData.filter(({TypeParams,ID})=>TypeParams==='9- Planned Order Receipt' && ID === infos.code)
      if(pors.length>0){
        var lot
        var exo = []
        var porCopy = []
        for(var i = 0;i<pors.length;i++){
          porCopy.push(pors[i])
        }
        var ndx = 0;
        var nbr = 2;
        var finalCopy = porCopy.splice(ndx, nbr);
       
        for(var i = 0;i<porCopy.length;i++){
          if(porCopy[i].Amount >= 0){
            var por2 = {
              'ID': infos.code,
              'Amount': porCopy[i].Amount,
              'Article': infos.designation,
              'TypeParams': 'a- Planner Order Release',
              'Year': previsions[i].Year,
              'Month':  previsions[i].Month,
            } 
            this.newData.push(por2)
            exo.push(por2)
          }
         
        }
        // console.log('la premiere POR est de ::!:: ',pors)
        var porsd = this.newData.filter(({TypeParams,ID})=>TypeParams==='a- Planner Order Release' && ID === infos.code)
        // console.log('la deuziemme POR est de ::!:: ',porsd)
        // var porT = this.newData.filter(({TypeParams,ID})=>TypeParams==='a- Planner Order Release' && ID === infos.code)
        // var res = 12 - porT.length
        // for(var i = 0;i<res;i++){
        //   this.newData.push(exo[exo.length-1])
        // }
      }


      //STOCK FUNCTION HERE
    if(itemStock.length>0){
      var d = new Date()
      var month = d.getMonth()+1
      var year = d.getFullYear()
      var date 
      if(month<10){
        date = '0'+month+'-'+year
      }else{
        date = month+'-'+year
      }
      // console.log(date)
      if(this.newData.length > 0){
        const result = this.newData.filter(({TypeParams,ID})=>TypeParams==='1- Prévision' && ID === infos.code) 
        const cmdPlan = this.newData.filter(({TypeParams,ID})=>TypeParams==='6- Commande Planifiée' && ID === infos.code) 
        const cmdRec = this.newData.filter(({TypeParams,ID})=>TypeParams==='9- Planned Order Receipt' && ID === infos.code) 
        const dmd = this.newData.filter(({TypeParams,ID})=>TypeParams==='5- Demande' && ID === infos.code) 
        // console.log('alrs les resu ',result)
        // console.log('les stocks ',itemStock)
        for(var k =0;k<12;k++){
          //  var cm = itemStock.filter(({Mois,Year})=> Mois === result[k].Mois && Year === result[k].Year )
        var cm = itemStock
          if(cm.length > 0 && k == 0 ){
            // console.log(cm)
            if(cm.Mois>10){
              moisType = cm[0].Mois + '-' + cm[0].Year
            }else{
              moisType = '0'+cm[0].Mois + '-' + cm[0].Year
            }
            var qte = parseFloat(cm[0].quantite)+parseFloat(cmdPlan[0].Amount)+parseFloat(cmdRec[0].Amount)-parseFloat(dmd[0].Amount)
            var cmdo: any = {
              'ID': infos.code,
              'Amount': qte,
              'Article': infos.designation,
              'TypeParams': '7- Stock Réel',
              'Year': cm[0].Year,
              'Month':  moisType,
            }
            this.newData.push(cmdo)
            
          }else{
            if(result[k].Mois>10){
              moisType = result[k].Mois + '-' + result[k].Year
            }else{
            moisType = '0'+result[k].Mois + '-' + result[k].Year
            }
            var qte = parseFloat(cmdPlan[k].Amount)+parseFloat(cmdRec[k].Amount)-parseFloat(dmd[k].Amount)
            var cmf: any = {
              'ID': infos.code,
              'Amount':qte,
              'Article': infos.designation,
              'TypeParams': '7- Stock Réel',
              'Year': result[k].Year,
              'Month':  moisType,
              // 'Semaine': 'Semaine '+result[k].Week
            }
            this.newData.push(cmf)
          }
        }
      }
      
    }else{
      const result = this.newData.filter(({TypeParams,ID})=>TypeParams==='1- Prévision' && ID === infos.code) 
      const cmdPlan = this.newData.filter(({TypeParams,ID})=>TypeParams==='6- Commande Planifiée' && ID === infos.code) 
        const cmdRec = this.newData.filter(({TypeParams,ID})=>TypeParams==='9- Planned Order Receipt' && ID === infos.code) 
        const dmd = this.newData.filter(({TypeParams,ID})=>TypeParams==='5- Demande' && ID === infos.code) 
      for(var k =0;k<12;k++){
        //  var cm = itemStock.filter(({Mois,Year})=> Mois === result[k].Mois && Year === result[k].Year )
        if(result[k].Mois>10){
          moisType = result[k].Mois + '-' + result[k].Year
        }else{
        moisType = '0'+result[k].Mois + '-' + result[k].Year
        }
        var stocky = this.newData.filter(({TypeParams,ID})=>TypeParams === "7- Stock Réel" && ID === infos.code)
        var stockPrevQte = (stocky.length>0 && k>0)?parseFloat(stocky[k-1].Amount):0
        var qte = stockPrevQte + parseFloat(cmdPlan[k].Amount)+parseFloat(cmdRec[k].Amount)-parseFloat(dmd[k].Amount)
        var cmf: any = {
          'ID': infos.code,
          'Amount':qte,
          'Article': infos.designation,
          'TypeParams': '7- Stock Réel',
          'Year': result[k].Year,
          'Month':  moisType,
          // 'Semaine': 'Semaine '+result[k].Week
        }
        this.newData.push(cmf)
      }
    }


    }

    //Stock de Securité
    if (itemPrev2.length > 0 ) {
      var dmds = []
      var prevs = []
      var commandesNext = this.newData.filter(({TypeParams,ID})=>TypeParams === '5- Demande' && ID === infos.code)
      // console.log("la commande Next est de :: ",commandesNext)
      for(var i=0;i<6;i++){
        var dm = itemCmd2.filter(({Mois,Year})=>Mois === this.stockDateRange[i].mois && Year === this.stockDateRange[i].year)
        // console.log('itemCMD2 :: ',dm)
        if(dm.length>0){
          dmds.push(dm[0].Sumventes)
        }else{
          dmds.push(0)
        }
      }
      // console.log('final ITEMCMD :: ',dmds)
      // console.log('stockRanged Data : ',this.stockDateRange)
      for(var i=0;i<this.stockDateRange.length;i++){
        var pr = itemPrev2.filter(({Mois,Year})=>Mois === this.stockDateRange[i].mois && Year === this.stockDateRange[i].year)
        if(pr.length>0){
          prevs.push(pr[0].prevision)
        }else{
          prevs.push(0)
        }
      }
      // console.log('voici les prev et les demsnde  ????',prevs,dmds)
      for(var i = 0;i<12;i++){
        if(i==0){
          var madMois = 0
          var abs = 0
          for (var k = 0; k < 6; k++) {
            abs = abs + Math.abs(parseFloat(dmds[k]) - parseFloat(prevs[k]))
          }
          madMois = abs / 6
          var ss = madMois*itemTauxSecurite
          var stockSecurite = {
            'ID': infos.code,
            'Amount': Math.round(ss),
            'Article': infos.designation,
            'TypeParams': '15- Stock Sécurité',
            'Year': commandesNext[0].Year,
            'Month': commandesNext[0].Month,
            'Mois':commandesNext[0].Mois,
          }
          this.newData.push(stockSecurite)
        }else{
          var a = 0
          var madMois = 0
          var abs = 0
          if(i<= 5){
            for (var k = i; k < 6; k++) {
              abs = abs + Math.abs(parseFloat(dmds[k]) - parseFloat(prevs[k]))
            }
            for (var k = 0; k < i; k++) {
                abs = abs + Math.abs(parseFloat(commandesNext[k+1].Amount) - parseFloat(prevs[k+6]))
            }
          }
          else{
            for (var k = 1; k <= 6; k++) {
                abs = abs + Math.abs(parseFloat(commandesNext[k].Amount) - parseFloat(itemPrev2[k+5].prevision))
            }
          }
          

          madMois = abs / 6
          var ss = madMois*itemTauxSecurite
          var stockSecurite = {
            'ID': infos.code,
            'Amount': Math.round(ss),
            'Article': infos.designation,
            'TypeParams': '15- Stock Sécurité',
            'Year': commandesNext[i].Year,
            'Month': commandesNext[i].Month,
            'Mois':commandesNext[i].Mois,
          }
          this.newData.push(stockSecurite)
        }
      }
    }
  }
  }
