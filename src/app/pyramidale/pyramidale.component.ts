import { Component, OnInit,ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ILoadedEventArgs, ChartTheme } from '@syncfusion/ej2-angular-charts';
import { DialogComponent, ButtonPropsModel } from '@syncfusion/ej2-angular-popups';
import { GridComponent,ToolbarItems,PageSettingsModel } from '@syncfusion/ej2-angular-grids';
import { ClickEventArgs } from '@syncfusion/ej2-navigations'
import * as XLSX from 'xlsx';


@Component({
  selector: 'app-pyramidale',
  templateUrl: './pyramidale.component.html',
  styleUrls: ['./pyramidale.component.scss']
})
export class PyramidaleComponent implements OnInit {
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + window.localStorage.getItem('ESP_access_token')
    })
  }

  @ViewChild('grid')
  public grid: GridComponent;

  constructor(private http: HttpClient) { }
  toolbar: ToolbarItems[] | object;
  ngOnInit() {
    this.toolbar = [
      'CsvExport',
      'ExcelExport',
      'Print',
      'Search'
    ];
    this.getObjectifsRange()
    
    this.getObjectifs(this.defaultData())
  }


  public chartArea: Object = {
    border: {
        width: 0
    }
};
public load(args: ILoadedEventArgs): void {
  let selectedTheme: string = location.hash.split('/')[1];
  selectedTheme = selectedTheme ? selectedTheme : 'Material';
  args.chart.theme = <ChartTheme>(selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1)).replace(/-dark/i, "Dark");
};
// custom code end
public title: string = 'Graphe Objectifs et Réalisations par famille';
  public width='10%';
    public data: Object[] = [
        { x: 'Egg', y: 2.2 }, { x: 'Fish', y: 2.4 },
        { x: 'Misc', y: 3 }, { x: 'Tea', y: 3.1 }
    ];
    public data1: Object[] = [
        { x: 'Egg', y: 1.2 }, { x: 'Fish', y: 1.3 },
        { x: 'Misc', y: 1.5 }, { x: 'Tea', y: 2.2 }
    ];
    //Initializing Marker
    public marker: Object = {
        dataLabel: {
            visible: true,
            position: 'Top',
            font: {
                fontWeight: '600', color: '#ffffff'
            }
        }
    }
    //Initializing Primary X Axis
    public primaryXAxis: Object = {
        valueType: 'Category',
        title: 'Prévision Pyramidale',
        interval: 1,
        majorGridLines: { width: 0 }
    };
    //Initializing Primary Y Axis
    public primaryYAxis: Object = {
        labelFormat: '{value}',
        edgeLabelPlacement: 'Shift',
        majorGridLines: { width: 0 },
        majorTickLines: { width: 0 },
        lineStyle: { width: 0 },
        labelStyle: {
            color: 'transparent'
        }
    };
    public tooltip: Object = {
        enable: true
    };


defaultData(){
  var d = new Date()
  var obDate = d.getFullYear()+'-'+(d.getMonth()+1)+'-15'
  return obDate
}

dateMix(){
  var d = new Date()
  var debut
  var fin
  var s = this.selectedOb-5
  if(s<0){
    debut = (this.yearDate()-1)+'-'+(12+s)+'-01'
    if(this.selectedOb == 0){
      fin = (this.yearDate()-1)+'-12-31'
    }else{
      fin = this.yearDate()+'-'+this.selectedOb+'-31'
    }
  }else if(s == 0){
    debut = (this.yearDate()-1)+'-12-01'
    fin = this.yearDate()+'-'+this.selectedOb+'-31'
  }
  else{
    debut = this.yearDate()+'-'+(s)+'-01'
    fin = this.yearDate()+'-'+this.selectedOb+'-31'
  }
  return {
    debut:debut,
    fin:fin
  }
}

defaultSelectedOb(){
  var d = new Date()
  return d.getMonth()
}
test = false
selectedMonth = this.sd()
number = 1300000
year: any = 0
objectifs = [150,100,120,140,250,320,256,541]

sd(){
  var d = new Date()
  var m = (d.getMonth()+1)>9?(d.getMonth()+1):('0'+(d.getMonth()+1))
  return d.getFullYear()+'-'+m
}


rangeObjectif: any = [0,0,0,0,0,0,0,0,0,0,0,0]
rangeRealisation: any = [0,0,0,0,0,0,0,0,0,0,0,0]
objectifTotal = 0
getObjectifsRange(){
  this.rangeObjectif = [0,0,0,0,0,0,0,0,0,0,0,0]
  this.rangeRealisation = ['...','...','...','...','...','...','...','...','...','...','...','...']
  let postData = new FormData();
  var d= new Date()
  var s = new Date()
  s.setDate(d.getDate()-5)
  var dateStock = s.getFullYear()+'-'+(s.getMonth()+1)+'-'+s.getDay()
  console.log('la dte :: ',dateStock)
  var debut = this.yearDate()+"-01-01"
  var fin = this.yearDate()+"-12-31"
  
  postData.append('dateDebut',debut);
  postData.append('dateFin',fin);
  this.http.post<any>('https://8000-sapphire-caterpillar-laqzwe8r.ws-eu18.gitpod.io/api/objectifs',postData, this.httpOptions).map(res => res).subscribe(data => {
   console.log(data)
   if(data.objectifs.length>0){
     for(var i =0;i<data.objectifs.length;i++){
        this.rangeObjectif[(data.objectifs[i].Mois-1)] = data.objectifs[i].objectifSum
     }
   }
   if(data.commandes.length>0){
     for(var i =0;i<data.commandes.length;i++){
        this.rangeRealisation[(data.commandes[i].Mois-1)] = parseFloat(data.commandes[i].Sumventes)
     }
   }
  }, err => {
    console.log(JSON.stringify(err));
  });
}

arraySum(x){
  const reducer = (a, b) => a.objectif + b.objectif;
  console.log('reducer :: ',reducer)
  return x.reduce(reducer)
}



dataDetails: any
totalDetails: any = 0
finalData: any
finalDataChart: any
familleTotals: any
getObjectifs(x){
  this.familleTotals = []
  this.dataDetails = []
  this.finalData = []
  this.finalDataChart = []
  this.objectifTotal = 0
  let postData = new FormData();
  var d= new Date()
  var s = new Date()
  s.setDate(d.getDate()-5)
  var dateStock = s.getFullYear()+'-'+(s.getMonth()+1)+'-'+s.getDay()
  console.log('la dte :: ',dateStock)
  var debut = this.yearDate()+'-'+(this.selectedOb+1)+"-01"
  var fin = this.yearDate()+'-'+(this.selectedOb+1)+"-31"
  
  postData.append('dateDebut',debut);
  postData.append('dateFin',fin);
  postData.append('dateDebutMix',this.dateMix().debut);
  postData.append('dateFinMix',this.dateMix().fin);
  postData.append('dateObjectif',x);
  this.http.post<any>('https://8000-sapphire-caterpillar-laqzwe8r.ws-eu18.gitpod.io/api/objectifs/details',postData, this.httpOptions).map(res => res).subscribe(data => {
   console.log(data)
   var commandes = data.commandes
   var objectifs = data.objectifs
   var familles = data.familles
   var mix = data.mix
   var mixTotal = parseFloat(data.mixTotal[0].Sumventes)
   var finalData = []
   var finalDataChart = []
   var totCmds = 0
   var totObj = 0
   for(var i=0;i<commandes.length;i++){
      totCmds = totCmds+parseFloat(commandes[i].Sumventes)
   }
   for(var i=0;i<objectifs.length;i++){
    totObj = totObj+parseFloat(objectifs[i].objectif)
   }
   this.familleTotals = [
     {
       commandes:totCmds,
       objectifs:totObj
     }
   ]
   for(var i = 0;i<familles.length;i++){
     var cmd = commandes.filter(({famille_id})=>famille_id === familles[i].id)
     var ob = objectifs.filter(({famille_id})=>famille_id === familles[i].id)
     var m = mix.filter(({famille_id})=>famille_id === familles[i].id)
     var x
     var y
     x = {
      famille_id:familles[i].id,
      cause:ob.length>0?ob[0].cause:'...',
      famille:familles[i].famille,
      objectif:ob.length>0?parseFloat(ob[0].objectif):0,
      realisation:cmd.length>0?parseFloat(cmd[0].Sumventes):'...',
      mix:m.length>0?(100*parseFloat(m[0].Sumventes)/mixTotal).toFixed(2):0
      }
      y = {
        famille:familles[i].famille,
        objectif:ob.length>0?ob[0].objectif:0,
        realisation:cmd.length>0?parseFloat(cmd[0].Sumventes):0
    }
    finalData.push(x)
    finalDataChart.push(y)
     
    //  if(cmd.length>0 && ob.length >0){
    //     x = {
    //       famille_id:familles[i].id,
    //       cause:ob[0].cause,
    //       famille:familles[i].famille,
    //       objectif:parseFloat(ob[0].objectif),
    //       realisation:parseFloat(cmd[0].Sumventes),
    //       mix:(100*parseFloat(m[0].Sumventes)/mixTotal).toFixed(2)
    //     }
    //     y = {
    //       famille:familles[i].famille,
    //       objectif:ob[0].objectif,
    //       realisation:parseFloat(cmd[0].Sumventes)
    //     }
    //     finalData.push(x)
    //     finalDataChart.push(y)
    //  }else if(cmd.length>0){
    //     x = {
    //       famille_id:familles[i].id,
    //       famille:familles[i].famille,
    //       cause:ob[0].cause,
    //       objectif:'....',
    //       realisation:parseFloat(cmd[0].Sumventes),
    //       mix:(100*parseFloat(m[0].Sumventes)/mixTotal).toFixed(2)
    //     }
    //     y = {
    //       famille:familles[i].famille,
    //       objectif:0,
    //       realisation:parseFloat(cmd[0].Sumventes)
    //     }
    //     finalData.push(x)
    //     finalDataChart.push(y)
    //  }else if( ob.length >0){
    //     x = {
    //       famille_id:familles[i].id,
    //       famille:familles[i].famille,
    //       cause:ob[0].cause,
    //       objectif:parseFloat(ob[0].objectif),
    //       realisation:'....',
    //       mix:(100*parseFloat(m[0].Sumventes)/mixTotal).toFixed(2)
    //     }
    //     y = {
    //       famille:familles[i].famille,
    //       objectif:ob[0].objectif,
    //       realisation:0
    //     }
    //     finalData.push(x)
    //     finalDataChart.push(y)
    //  }else{
    //     x = {
    //       famille_id:familles[i].id,
    //       famille:familles[i].famille,
    //       cause:ob[0].cause,
    //       objectif:'....',
    //       realisation:'....',
    //       mix:(100*parseFloat(m[0].Sumventes)/mixTotal).toFixed(2)
    //     }
    //     y = {
    //       famille:familles[i].famille,
    //       objectif:0,
    //       realisation:0
    //     }
    //     finalData.push(x)
    //     finalDataChart.push(y)
    //  }
   }

   this.finalData = finalData
   this.finalDataChart = finalDataChart
   console.log('chart',this.finalData)

  //  this.dataDetails = data
  //  this.totalDetails = 0
  //  if(data.length>0){
  //    for(var i=0;i<data.length;i++){
  //      this.totalDetails = this.totalDetails + data[i].objectif
  //    }
  //  }
  }, err => {
    console.log(JSON.stringify(err));
  });
}

percentCalc(obj){
  var res = (obj/this.totalDetails)*100
  return res.toFixed(2)
}

dateRange(x){
  var d = new Date()
  var dates = []
  for(var i =1 ;i<=12;i++){
    if(i<10){
      var s = (d.getFullYear()+x)+'-0'+i
      dates.push(s)
    }else{
      var s = (d.getFullYear()+x)+'-'+i
      dates.push(s)
    }
  }
  return dates
}

yearDate(){
  var d = new Date()
  var year = this.year + d.getFullYear()
  return year
}

nextYear(){
  this.year = this.year+1
  this.getObjectifsRange()
}
previousYear(){
  this.year = this.year-1
  this.getObjectifsRange()
}

CurrencyFormat(number)
{
   return number.toLocaleString()
}

selectedOb = this.defaultSelectedOb()
selectObjectif(i){
  this.test = true
  this.selectedOb = i
  this.selectedMonth = this.dateRange(this.year)[i]
  console.log('index ',i)
  console.log('date selected : ',this.dateRange(this.year)[i])
  var c = this.selectedMonth+'-15'
  this.getObjectifs(c)
  this.showInputIndex = 100
}

showInputIndex = 100
showInputObGlobal(index,value){
  var d = new Date()
  var m = d.getMonth()
  if(index>= m && d.getFullYear()>= this.yearDate() &&  this.selectedOb == index){
    this.showInputIndex = index
    this.globalInputValue = value
  }
  
}

globalInputValue: any
confirmOb(){
  var data = []
  console.log(this.globalInputValue)
  var c = this.selectedMonth+'-15'
  this.saveObjectif(c,this.globalInputValue)
  console.log('data MIXED ',data)
}



saveObjectif(x,objectif){
  this.dataDetails = []
  this.finalData = []
  this.finalDataChart = []
  let postData = new FormData();
  var d= new Date()
  var s = new Date()
  s.setDate(d.getDate()-5)
  var dateStock = s.getFullYear()+'-'+(s.getMonth()+1)+'-'+s.getDay()
  console.log('la dte :: ',dateStock)
  var debut = this.yearDate()+'-'+(this.selectedOb+1)+"-01"
  var fin = this.yearDate()+'-'+(this.selectedOb+1)+"-31"
  
  postData.append('dateDebut',debut);
  postData.append('dateFin',fin);
  postData.append('dateDebutMix',this.dateMix().debut);
  postData.append('dateFinMix',this.dateMix().fin);
  postData.append('dateObjectif',x);
  postData.append('objectif',objectif);
  this.http.post<any>('https://8000-sapphire-caterpillar-laqzwe8r.ws-eu18.gitpod.io/api/objectifs/add',postData, this.httpOptions).map(res => res).subscribe(data => {
   console.log('edition ou ajout done')
   this.rangeObjectif[this.selectedOb]= objectif
   this.showInputIndex = 100
   this.getObjectifs(x)
  }, err => {
    console.log(JSON.stringify(err));
  });
}

selectedType: any = ''
sortFamille(type){
  console.log(this.finalData)
  switch(type) {
    case 'famille':
      this.selectedType = 'famille'
      this.finalData.sort(function(a, b) {
        var nameA = a.famille.toUpperCase(); // ignore upper and lowercase
        var nameB = b.famille.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        return 0;
      });
      break;
    case 'mix':
      this.selectedType = 'mix'
      this.finalData.sort(function (a, b) {
        return a.mix - b.mix;
      });
      break;
    case 'objectif':
      this.selectedType = 'objectif'
      this.finalData.sort(function (a, b) {
        return a.objectif - b.objectif;
      });
      break;
    case 'realisation':
      this.selectedType = 'realisation'
      this.finalData.sort(function (a, b) {
        return a.realisation-b.realisation;
      });
      break;
    default:
      return this.finalData
  }
}

//FAMILLE EDIT MODAL
@ViewChild('Dialog')
    public Dialog: DialogComponent;
    selectedFamille = ''
    public headerM: string = 'Objectif Famille '+this.selectedFamille+' / '+this.selectedMonth;
    public showCloseIconM: Boolean = true;
    public widthM: string = '30%';
    public targetM: string = '.control-section';
    public dialogClose = (): void => {
      this.Dialog.hide()
    }
    // On Dialog open, 'Open' Button will be hidden
   

  public FamilleClick = (famille): void => {
      console.log(famille)
      this.getFamilleObjectif(famille)
      this.headerM = famille+' / '+this.selectedMonth;
      var d = new Date()
      var m = d.getMonth()
      if(this.selectedOb >= m){
        this.Dialog.show();
      }
     
  }

  familleInputObjectif: any = 0
  familleInputCause = ''
  familleInputID: any
  getFamilleObjectif(famille){
    this.familleInputObjectif = 0
    this.familleInputCause = ''
    let postData = new FormData();
    let dat = this.selectedMonth+'-15'
    postData.append('date',dat);
    postData.append('famille',famille);
    this.http.post<any>('https://8000-sapphire-caterpillar-laqzwe8r.ws-eu18.gitpod.io/api/objectifs/famille/get',postData, this.httpOptions).map(res => res).subscribe(data => {
      console.log('obb ',data)
      this.familleInputObjectif = data.objectif
      this.familleInputCause = data.cause
      this.familleInputID = data.id
    }, err => {
      console.log(JSON.stringify(err));
    });
  }

  editFamilleObjectif(){
    let postData = new FormData();
    let dat = this.selectedMonth+'-15'
    postData.append('id',this.familleInputID);
    postData.append('objectif',this.familleInputObjectif);
    postData.append('cause',this.familleInputCause);
    this.http.post<any>('https://8000-sapphire-caterpillar-laqzwe8r.ws-eu18.gitpod.io/api/objectifs/famille/edit',postData, this.httpOptions).map(res => res).subscribe(data => {
      console.log('saved edit ',data)
      this.getObjectifs(dat)
      this.Dialog.hide()
    }, err => {
      console.log(JSON.stringify(err));
    });
  }

  @ViewChild('DialogArticle')
    public DialogArticle: DialogComponent;
    public headerA: string = 'Objectif Famille '+this.selectedFamille+' / '+this.selectedMonth;
    public showCloseIconA: Boolean = true;
    public widthA: string = '85%';
    public targetA: string = '.control-section';
    public dialogCloseA = (): void => {
      this.DialogArticle.hide()
    }
    // On Dialog open, 'Open' Button will be hidden
    selectedFamilleT: any
   showArticle(id){
     this.DialogArticle.show()
     var date = this.selectedMonth+'-15'
     this.selectedFamilleT = id
     this.getArticleObjectifs(date,id)
   }




   dataArticleDetails: any
   totalArticleDetails: any = 0
   finalArticleData: any
   finalArticleDataChart: any
   familleArticleTotals: any
   objectifArticleTotal: any
   getArticleObjectifs(x,famille_id){
     this.familleArticleTotals = []
     this.dataArticleDetails = []
     this.finalArticleData = []
     this.finalArticleDataChart = []
     this.objectifArticleTotal = 0
     let postData = new FormData();
     var d= new Date()
     var s = new Date()
     s.setDate(d.getDate()-5)
     var dateStock = s.getFullYear()+'-'+(s.getMonth()+1)+'-'+s.getDay()
     console.log('la dte :: ',dateStock)
     var debut = this.yearDate()+'-'+(this.selectedOb+1)+"-01"
     var fin = this.yearDate()+'-'+(this.selectedOb+1)+"-31"
     
     postData.append('dateDebut',debut);
     postData.append('dateFin',fin);
     postData.append('dateDebutMix',this.dateMix().debut);
     postData.append('dateFinMix',this.dateMix().fin);
     postData.append('dateObjectif',x);
     postData.append('famille_id',famille_id);
     this.http.post<any>('https://8000-sapphire-caterpillar-laqzwe8r.ws-eu18.gitpod.io/api/objectifs/articles/details',postData, this.httpOptions).map(res => res).subscribe(data => {
      console.log(data)
      var commandes = data.commandes
      var objectifs = data.objectifs
      var articles = data.articles
      var stocks = data.stocks
      var familleObjetif = parseFloat(data.objectifFamille[0].objectif)
      var mix = data.mix
      var mixTotal = parseFloat(data.mixTotal[0].Sumventes)
      var finalData = []
      var finalDataChart = []
      var totCmds = 0
      var totObj = 0
      for(var i =0;i<articles.length;i++){
        var cmd = commandes.filter(({article_id})=>article_id === articles[i].id)
        var m = mix.filter(({article_id})=>article_id === articles[i].id)
        var s = stocks.filter(({article_id})=>article_id === articles[i].id)
        var ob = objectifs.filter(({article_id})=>article_id === articles[i].id)
        var obje = ob.length>0?parseFloat(ob[0].objectif):(m.length>0?Number((familleObjetif*parseFloat(m[0].Sumventes)/mixTotal).toFixed(2)):1)
        var real = cmd.length>0?parseFloat(cmd[0].Sumventes):0
        var stock = s.length>0?parseFloat(s[0].stock):0

        var t = real/obje
        var x = {
          id:articles[i].id,
          code:articles[i].code,
          designation:articles[i].designation,
          qte:cmd.length>0?parseFloat(cmd[0].qte):0,
          realisation:cmd.length>0?parseFloat(cmd[0].Sumventes):0,
          mix:m.length>0?Number((100*parseFloat(m[0].Sumventes)/mixTotal).toFixed(2)):0,
          objectif:ob.length>0?parseFloat(ob[0].objectif):(m.length>0?Number((familleObjetif*parseFloat(m[0].Sumventes)/mixTotal).toFixed(2)):0),
          objectifExist:ob.length>0?true:false,
          tau:parseFloat((t*100).toFixed(2)),
          stock:stock

        }
        finalData.push(x)
      }
      
      this.finalArticleData = finalData
      console.log('Article DATA',this.finalArticleData)
   
     //  this.dataDetails = data
     //  this.totalDetails = 0
     //  if(data.length>0){
     //    for(var i=0;i<data.length;i++){
     //      this.totalDetails = this.totalDetails + data[i].objectif
     //    }
     //  }
     }, err => {
       console.log(JSON.stringify(err));
     });
   }


   toExportFileName(excelFileName: string): string {
    return `${excelFileName}_export_${new Date().getTime()}.xlsx`;
  }
  
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {Sheets: {'data': worksheet}, SheetNames: ['data']};
    XLSX.writeFile(workbook, this.toExportFileName(excelFileName));
  }

   toolbarClick(args: ClickEventArgs): void {
    switch (args.item.text) {
        case 'PDF Export':
            this.grid.pdfExport();
            break;
        case 'Excel Export':
           this.exportAsExcelFile(this.finalArticleData,'Relances')
            // this.grid.excelExport();
            break;
        case 'CSV Export':
            this.grid.csvExport();
            break;
    }
   } 


   selectedRow: any = 0
   newObjectif: any
   showInputRow(id,objectif){
     this.selectedRow = id
     this.newObjectif = objectif
   }

   saveArticleObjectif(articleid,lastObjectif){
      console.log('loo= ',articleid,lastObjectif,this.newObjectif)
      let postData = new FormData();
      var date = this.selectedMonth+'-15'
      postData.append('dateObjectif',date);
      postData.append('article_id',articleid);
      postData.append('famille_id',this.selectedFamilleT);
      postData.append('lastObjectif',lastObjectif);
      postData.append('newObjectif',this.newObjectif);
      this.http.post<any>('https://8000-sapphire-caterpillar-laqzwe8r.ws-eu18.gitpod.io/api/objectifs/articles/edit',postData, this.httpOptions).map(res => res).subscribe(data => {
       console.log(data)
        var index = this.finalArticleData.findIndex(({id})=>id === articleid)
        this.finalArticleData[index].objectif = this.newObjectif
        this.selectedRow = 0
        this.grid.refresh();
        this.getObjectifs(date)
        this.getObjectifsRange()

      }, err => {
        console.log(JSON.stringify(err));
      });

   }

}
